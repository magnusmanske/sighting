<?PHP

//error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);
//ini_set('display_errors', 'On');

include_once ( "php/common.php" ) ;

$sizediff = array () ;


function pluralPl( $count, $forms ) {
	if ( !count($forms) ) { return ''; }
#	$forms = $this->preConvertPlural( $forms, 3 );
	$count = abs( $count );
	if ( $count == 1 )
		return $forms[0];     // singular
	switch ( $count % 10 ) {
		case 2:
		case 3:
		case 4:
			if ( $count / 10 % 10 != 1 )
				return $forms[1]; // plural
		default:
			return $forms[2];   // plural genitive
	}
}

function db_get_out_of_sight ( $language , $category ) {
	global $last_revs , $last_ts , $db ;
	make_db_safe ( $category , true ) ;
	
	$reviewed_pages = array () ;
	$reviewed_current = array () ;
	$sql = "SELECT page_id,page_title,page_latest,fp_stable FROM page,categorylinks,flaggedpages WHERE cl_to=\"$category\" AND cl_from=page_id AND fp_page_id=page_id AND page_latest<>fp_stable AND page_namespace=0" ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
	while($o = $result->fetch_object()){
		$reviewed_pages[$o->page_id] = $o->page_title ;
			$last_revs[$o->page_title] = $o->fp_stable ;
	}
	return $reviewed_pages ;
}


function myenc ( $t ) {
	return  (  $t );
}

function get_note ( $title , $last ) {
	global $language , $sizediff , $simple ;
	$url_title = myenc ( $title ) ;
	$maxback = 20 ;
	$url = "http://$language.wikipedia.org/w/api.php?action=query&prop=revisions&titles=$url_title&rvlimit=$maxback&rvprop=user|timestamp|size|ids&format=php" ;
	$data = unserialize ( file_get_contents ( $url ) ) ;
	
	$data = $data['query'] ; 
	if ( !isset ( $data['pages'] ) ) return ' (ERROR)' ;
	$data = $data['pages'] ;
	if ( count ( $data ) == 0 ) return ' (ERROR)' ;
	$data = array_shift ( $data ) ;
	if ( !isset ( $data['revisions'] ) ) return ' (ERROR)' ;
	$data = $data['revisions'] ;
	if ( count ( $data ) == 0 ) return ' (ERROR)' ;
	
	$found = 0 ;
	$numrevs = 0 ;
	$cur_size = $data[0]['size'] ;
	$authors = array () ;
	$last_author = '' ;
	for ( $rev = 0 ; $rev < count ( $data ) ; $rev++ ) {
		$revid = $data[$rev]['revid'] ;
		if ( $revid == $last ) {
			$found = 1 ;
			$last_size = $data[$rev]['size'] ;
			break ;
		}
		$auth = $data[$rev]['user'] ;
		if ( $auth != $last_author ) array_unshift ( $authors , "<i>$auth</i>" ) ;
		$last_author = $auth ;
		$numrevs++ ;
#		print "$revid : $last<br/>" ;
	}
	if ( $found == 0 ) return " (more than $maxback revisions old)" ;
	
	$diff = $cur_size - $last_size ;
	$bg = abs ( $diff ) > 10 ? '#DDDDDD' : 'yellow' ;
	if ( abs ( $diff ) > 150 ) $bg = '#BBBBBB' ;
	$sizediff[$title] = $diff ;
	if ( $diff > 0 ) $diff = "<span style='color:green;background:$bg'>+$diff</span>" ;
	else if ( $diff < 0 ) $diff = "<span style='color:red;background:$bg'>$diff</span>" ;
	else $diff = "<span style='background:$bg'>$diff</span>" ;
	if ( $simple ) $ret = $diff ;
	else $ret = "$diff chars difference; $numrevs revisions since last sight; edited by " . implode ( ", " , $authors ) ;
	return " ($ret)" ;
	
#	print "<pre>";
#	print_r ( $data ) ;
#	print "</pre>";
}

$language = get_request ( 'language' , 'de' ) ;
$project = get_request ( 'project' , 'wikipedia' ) ;
$category = get_request ( 'category' , '' ) ;
$depth = get_request ( 'depth' , 1 ) ;
$mode = get_request ( 'mode' , '' ) ;
$extended = get_request ( 'extended' , 0 ) ;
$simple = get_request ( 'simple' , 0 ) ;
$sortby = get_request ( 'sortby' , 'category' ) ;

if ( $language == 'pl' ) $catname = 'Kategoria' ;
else $catname = 'Kategorie' ;

$db = openDB ( $language , $project ) ;


if ( $category == '' and $mode != 'rss' ) {

	if ( $language == 'pl' ) {
		$t1 = 'Pokazuje artykuły ze wskazanej kategorii i jej podkategorii, które oczekują na przejrzenie.' ;
		$t2 = 'Język' ;
		$t3 = 'Głębokość' ;
		$t4 = 'przeszukanie wskazanej kategorii oraz bezpośrednio należących do niej podkategorii' ;
		$t5 = 'Szukaj' ;
	} else {
		$t1 = 'Zeigt Artikel in (Unter)kategorien, die nachgesichtet werden müssen.' ;
		$t2 = 'Sprache' ;
		$t3 = 'Tiefe' ;
		$t4 = 'Kategorie und direkte Unterkategorien' ;
		$t5 = 'Los!' ;
	}

	print get_common_header ( '' , 'Deep out-of-sight' ) ;
	print "<i>$t1</i><br/>
	<form method='get' action='deep_out_of_sight.php'>
	<table class='table'>
	<tr><th>$t2</th><td style='width:100%'><input type='text' name='language' value='$language' size='30' /></td></tr>
	<tr><th>$catname</th><td><input type='text' name='category' value='' size='30' /></td></tr>
	<tr><th>$t3</th><td><input type='text' name='depth' value='$depth' /> (1=$t4)</td></tr>
	<tr><th></th><td><input type='submit' name='doit' value='$t5' class='btn btn-primary' /></td></tr>
	</table>
	</form>
	</body>" ;
	print get_common_footer() ;
	exit  ;
}

$data = getPagesInCategory ( $db , $category , $depth , 14 ) ;

if ( count ( $data ) == 0 ) {
	$category = utf8_encode ( $category ) ;
	$data = getPagesInCategory ( $db , $category , $depth , 14 ) ;
}


if ( !isset ( $data[$category] ) ) array_unshift ( $data , $category ) ;
//asort ( $data ) ;

if ( $mode == 'rss' ) {
	header('Content-type: text/xml; charset=utf-8');
	print '<?xml version="1.0" encoding="utf-8"?><rss version="2.0"><channel>' ;
	print '<title>' ;
	print "Nachzusichtende Artikel in Kategorie:" . myenc ( $category ) . ", Tiefe $depth (" . count ( $data ) . " Kategorien insgesamt)" ;
	print '</title>' ;
	print '<pubDate>' . date('r') . '</pubDate>' ;
	print '<ttl>5</ttl>' ;
	print '<link>' . htmlspecialchars ( "http://tools.wmflabs.org/sighting/deep_out_of_sight.php?category=$category&depth=$depth&language=$language" ) . '</link>' ;
	print "<language>$language-$language</language>" ;
} else {
	$add = ( $extended == 1 ) ? '&extended=1' : '' ;
	$l = $simple ? '' : '<link href="'."http://tools.wmflabs.org/sighting/deep_out_of_sight.php?category=".htmlspecialchars($category)."&language=$language&depth=$depth&mode=rss$add".'" rel="alternate" type="application/rss+xml" title="Diese Seite als RSS-Feed">' ;
	
	$style = '' ;
	if ( $simple ) $style = ' style="font-size:9pt"' ;
	print get_common_header ( '' , 'Deep out-of-sight' , array ( 'link' => $l ) ) ;
	print "<div $style>" ;
	if ( !$simple ) {
		if ( $language == 'pl' ) {
			print "Przeszukano " . count ( $data ) . " " . pluralPl ( count ( $data ) , array ( 'kategorię','kategorie','kategorii' ) ) . " w poszukiwaniu artykułów oczekujących na przejrzenie..." ;
		} else {
			print "Durchsuche " . count ( $data ) . " Kategorien auf nachzusichtende Artikel..." ;
		}
		myflush() ;
	}
	
	if ( $simple ) print '<ol style="padding-left:0px">' ;
	else print '<ol>' ;
}

$taburls = array () ;
$out2 = array () ;

$had_that = array () ;
foreach ( $data AS $d ) {
	$url = "http://$language.wikipedia.org/w/index.php?title=Special:PendingChanges&namespace=0&category=" . myurlencode ( $d ) ;
//	$pages = db_get_articles_in_category ( $language , $d , 0 , 0 ) ;
	$last_revs = array () ;
//	$last_ts = array () ;
	$oos = db_get_out_of_sight ( $language , $d ) ;
	foreach ( $oos AS $k => $v ) { // Show each article only once
		if ( isset ( $had_that[$v] ) ) unset ( $oos[$k] ) ;
	}
	if ( count ( $oos ) == 0 ) continue ;



	if ( $mode == 'rss' ) {
		$nd = str_replace ( '_' , ' ' , $d ) ;
		print '<item>' ;
		print '<title>' . myenc ( "$catname:$nd" ) . '</title>' ;
		print '<link>' ;
		print htmlspecialchars ( $url ) ;
		print '</link>' ;
		$out = '<ol>' ;
		$last = '' ;
		foreach ( $oos AS $t ) {
			$l = $last_revs[$t] ;
//			$ts = $last_ts[$t] ;
//			if ( $ts > $last ) $last = $ts ;
			if ( $extended ) $note = get_note ( $t , $l ) ; else $note = '' ;
			$out .= '<li><a target="_blank" href="http://' . $language . '.wikipedia.org/w/index.php?title=' . urlencode ( $t ) . "&oldid=$l&diffonly=0&diff=cur" . '">' ;
//			$out .= '<li><a target="_blank" ts="' . $ts . '" href="http://' . $language . '.wikipedia.org/w/index.php?title=' . urlencode ( $t ) . "&oldid=$l&diffonly=0&diff=cur" . '">' ;
			$out .= myenc ( $t ) . "</a>$note</li>" ;
			$had_that[$t] = 1 ;
		}
		$out .= '</ol>' ;
		$guid = myenc ( "$language:$d:" . md5 ( $out ) ) ;
		print "<guid>$guid</guid>" ;
		print "<description>" . htmlspecialchars ( $out ) . "</description>" ;
		print '</item>' ;
	} else if ( $sortby == 'size' ) {
		foreach ( $oos AS $t ) {
			$l = $last_revs[$t] ;
			$note = get_note ( $t , $l ) ;
			$key = sprintf ( "%5d %s" , $sizediff[$t] , $t ) ;
			$url = get_wikipedia_url ( $language , $t ) . "&diffonly=0&oldid=$l&diff=cur" ;
			$taburls[] = $url ;
			$list = '<li><a target="_blank" href="' . $url . '">' ;
			$list .= str_replace('_',' ',$t) . "</a>$note</li>" ;
			$out2[$key] = $list ;
			$had_that[$t] = 1 ;
		}
	} else {
		$list = '';
		if ( $simple ) $target = '_top' ;
		else $target = '_blank' ;
		foreach ( $oos AS $t ) {
			$l = $last_revs[$t] ;
			$note = '' ;
			if ( $extended ) $note = get_note ( $t , $l ) ;
			$url2 = get_wikipedia_url ( $language , $t ) . "&diffonly=0&oldid=$l&diff=cur" ;
			$taburls[] = $url2 ;
			$list .= '<li><a target="$target" href="' . $url2 . '">' ;
			$list .= str_replace('_',' ',$t) . "</a>$note</li>" ;
			$had_that[$t] = 1 ;
		}
		if ( $simple ) print $list ;
		else print "<li><a href=\"$url\" target='_blank'>$catname:$d</a><ol>$list</ol></li>" ;
		myflush () ;
	}
}


if ( $mode == 'rss' ) {
	print '</channel></rss>' ;
} else {
	if ( $sortby == 'size' ) {
//		print "<ol>" ;
		ksort ( $out2 ) ;
		foreach ( $out2 AS $l ) {
			print $l ;
		}
//		print "</ol>" ;
	}
	print '</ol><hr/>' ;
	if ( !$simple ) {
		$cht = count ( $had_that ) ;
		if ( $language == 'pl' ) {
			print "Znaleziono $cht " . pluralPl ( $cht , array ( 'artykuł oczekujący','artykuły oczekujące','artykułów oczekujących' ) ) . " na przejrzenie. " ;
		} else {
			print $cht . ' nachzusichtende Artikel gefunden. ' ;
		}
	}
	

	$out = '' ;
	print "<script type='text/javascript'>" ;

	if ( count ( $taburls ) > 20 ) {
		print "function open20 () {\n" ;
		for ( $a = 0 ; $a < 20 ; $a++ ) {
			print "window.open ('".$taburls[$a]."', '_blank');\n" ;

		}
		print "return false;};\n\n" ;
		$out .= "<a href='#' onclick='open20()'>Open first 20 in tabs</a> " ;
	}
	
	if ( count ( $taburls ) > 50 ) {
		print "function open50 () {\n" ;
		for ( $a = 0 ; $a < 50 ; $a++ ) {
			print "window.open ('".$taburls[$a]."', '_blank');\n" ;

		}
		print "return false;};\n\n" ;
		$out .= "<a href='#' onclick='open50()'>Open first 50 in tabs</a> " ;
	}

	print "function open_all() {\n" ;
	for ( $a = 0 ; $a < count($taburls) ; $a++ ) {
		print "window.open ('".$taburls[$a]."', '_blank');\n" ;
	}
	print "return false;};\n\n" ;
	$out .= "<a href='#' onclick='open_all()'>" ;
	if ( $language == 'pl' ) $out .= 'Otwórz wszystkie w kartach' ;
	else if ( $language == 'pl' ) $out .= 'Alle in Tabs öffnen' ;
	else $out .= "Open all in tabs" ;
	$out .= "</a> " ;
	
	print "</script>$out" ;
	
	print '</div>' ;
	print get_common_footer() ;
}

?>