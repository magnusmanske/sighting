<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);
ini_set('display_errors', 'On');

include_once ( "php/common.php" ) ;

$language = get_request ( 'language' , 'de' ) ;
$project = get_request ( 'project' , 'wikipedia' ) ;
$out = (int) get_request ( 'out' , 20 ) ;
$limit = (int) get_request ( 'limit' , 5 ) ;

print get_common_header ( "random_out_of_sight.php" , 'Random out-of-sight' ) ;

$db = openDB ( $language , $project ) ;

$quick_random = get_request ( 'qr' , 1 ) ;


$list = array () ;
$mtime = microtime(); 
$mtime = explode(' ', $mtime); 
$mtime = $mtime[1] + $mtime[0]; 
$starttime = $mtime; 

$sql = "SELECT page_title,page_len,rev_len,page_latest,rev_id FROM page,flaggedpages,revision WHERE page_id IN ( SELECT page_id FROM page,flaggedpages WHERE page_namespace=0 AND fp_page_id=page_id AND page_latest<>fp_stable) AND fp_page_id=page_id AND rev_id=fp_stable AND (cast(page_len as signed)-cast(rev_len as signed)) BETWEEN -$limit AND $limit ORDER BY rand() LIMIT $out" ;
if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
while($o = $result->fetch_object()){
	$list[] = $o ;
}

$mtime = microtime(); 
$mtime = explode(" ", $mtime); 
$mtime = $mtime[1] + $mtime[0]; 
$endtime = $mtime; 
$totaltime = ($endtime - $starttime); 

shuffle ( $list ) ;
$taburls = array () ;
print "<ol>" ;
for ( $a = 0 ; $a < $out ; $a++ ) {
	$o = array_shift ( $list ) ;
	$name = str_replace ( '_' , ' ' , $o->page_title ) ;
	$diff = $o->page_len - $o->rev_len ;
	$col = 'black' ;
	if ( $diff < 0 ) $col = 'red' ;
	if ( $diff > 0 ) $col = 'green' ;
	if ( $diff == 0 ) $diff = "&plusmn;0" ;
	$url = "http://$language.$project.org/w/index.php?title=" . myurlencode($o->page_title) . "&oldid={$o->rev_id}&diff=cur&diffonly=0" ;
	$taburls[] = $url ;
	print "<li>" ;
	print "<a target='_blank' href=\"$url\">$name</a> (<span style='color:$col'>$diff</span> chars)" ;
	print "</li>" ;
}
print "</ol>" ;

print "<script type='text/javascript'>" ;
print "function open_all() {\n" ;
foreach ( $taburls AS $t ) {
	print "window.open ('$t', '_blank');\n" ;
}
print "return false;};\n\n" ;
print "</script>" ;
print "<hr/><a href='#' onclick='open_all()'>Open all in tabs</a> " ;


print get_common_footer() ;
