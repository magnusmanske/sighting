<?PHP

include ( "php/common.php") ;

function which_articles_in_category ( &$titles , $category ) {
	global $language ;
	
	$t = array () ;
	foreach ( $titles AS $k => $v ) {
		$nk = array_pop ( explode ( '?title=' , $v , 2 ) ) ;
		$nk = array_shift ( explode ( '&' , $nk , 2 ) ) ;
		$nk = urldecode ( $nk ) ;
		make_db_safe ( $nk ) ;
		$t[$nk] = $v ;
	}
	
	
	
	make_db_safe ( $category ) ;
	$ret = array () ;
	$mysql_con = db_get_con_new($language) ;
	$db = $language . 'wiki_p' ;
	$ret = array () ;
	
	while ( count ( $t ) > 0 ) {
		$t2 = array () ;
		foreach ( $t AS $k => $v ) {
			$t2[$k] = $v ;
			unset ( $t[$k] ) ;
			if ( count ( $t ) == 0 or count ( $t2 ) >= 400 ) break ;
		}
		$sql = "SELECT page_title FROM page,categorylinks WHERE cl_from=page_id AND cl_to=\"$category\" AND page_title IN (\"" ;
		$sql .= implode ( '","' , array_keys($t2) ) ;
		$sql .= '")' ;
	#	print "$sql<br/>" ;
	#	print "<pre>" ;
	#	print_r ( $t ) ;
	#	print "</pre>" ;
	#	return array();
	
		$res = mysql_db_query ( $db , $sql , $mysql_con ) ;
		if ( mysql_errno() != 0 ) return $ret ; // Something's broken
	
		while ( $o = mysql_fetch_object ( $res ) ) {
			$ret[] = $t2[$o->page_title] ;
		}
	}
	return $ret ;
}



$language = 'de' ;
$category = get_request ( 'category' , '' ) ;

$text = file_get_contents ( 'http://toolserver.org/~aka/cgi-bin/reviewcnt.cgi?lang=german&action=outofdatereviews' ) ;
preg_match_all ( "/href='(http:\/\/de\.wikipedia\.org\/w\/[^']+)'/" , $text , $matches ) ;
$matches = $matches[1] ;

$msg = '' ;
if ( $category != '' ) {
	$m2 = which_articles_in_category ( $matches , $category ) ;
	if ( count ( $m2 ) > 0 ) {
		$matches = $m2 ;
		$msg = count ( $matches ) . " pages with out-of-date sightings in category $category<br/>" ;
	} else {
		$msg = "No pages with out-of-date sightings in category $category, using full list<br/>" ;
	}
}

$pos = rand ( 0 , count ( $matches ) - 1 ) ;
$url = $matches[$pos] ;

print get_common_header ( '' , "Out of sight" ) ;

print "This tool is deactivated, because it depends on <a href='http://toolserver.org/~aka/cgi-bin/reviewcnt.cgi'>Aka</a>'s tool, which is gone now." ; exit ( 0 ) ;

print "$msg
<form method='get'>
<input type='hidden' name='category' value='$category' />
<input type='submit' value='Another one' />
<!-- (also see the <a href='http://toolserver.org/~aka/cgi-bin/reviewcnt.cgi?lang=german&action=outofdatereviews'>full list</a> with out-of-date reviews)-->
</form>
<iframe width='100%' height='90%' src='$url'/>" ;
print get_common_footer() ;
?>